# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150407192758) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "merchants", force: :cascade do |t|
    t.string   "name"
    t.string   "location"
    t.integer  "rating"
    t.integer  "gst"
    t.integer  "servicecharge"
    t.string   "contactinfo"
    t.string   "companylogo_file_name"
    t.string   "companylogo_content_type"
    t.integer  "companylogo_file_size"
    t.datetime "companylogo_updated_at"
    t.string   "companylogo_path"
    t.string   "usercomments"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "receipts", force: :cascade do |t|
    t.string   "receipt_file_name"
    t.string   "receipt_content_type"
    t.integer  "receipt_file_size"
    t.datetime "receipt_updated_at"
    t.string   "receipt_path"
    t.integer  "imageable_id"
    t.string   "imageable_type"
  end

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.string   "avatar_path"
    t.string   "email"
    t.integer  "reputation"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

end
