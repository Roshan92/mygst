class CreateReceipts < ActiveRecord::Migration
  def change
    create_table :receipts do |t|
    	t.attachment :receipt
      	t.string :receipt_path
    	t.references :imageable, polymorphic: true
    end
  end
end
