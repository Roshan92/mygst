class CreateMerchants < ActiveRecord::Migration
  def change
    create_table :merchants do |t|
      t.string :name
      t.string :location
      t.integer :rating
      t.integer :gst
      t.integer :servicecharge
      t.string :contactinfo
      t.attachment :companylogo
      t.string :companylogo_path
      t.string :usercomments

      t.timestamps null: false
    end
  end
end
