class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name
      t.attachment :avatar
      t.string :avatar_path
      t.string :email
      t.integer :reputation

      t.timestamps null: false
    end
  end
end
