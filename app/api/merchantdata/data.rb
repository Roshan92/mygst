module Merchantdata
  class Data < Grape::API
    resource :merchantdata_data do
      desc "list"
      get do
        Merchant.all
      end
 
      desc "create a new merchant"
      params do
        requires :name, type: String
        requires :location, type:String
        requires :servicecharge, type:String
        requires :gst, type:String
        requires :contactinfo, type:String
      end
      post do
        Merchant.create!({
          name:params[:name],
          location:params[:location],
          servicecharge:params[:servicecharge],
          gst:params[:gst],
          contactinfo:params[:contactinfo]
        })
      end

      desc "delete a Merchant"
      params do
        requires :id, type: String 
      end
      delete ':id' do
        Merchant.find(params[:id]).destroy!
      end

      desc "update a Merchant"
      params do 
        requires :id, type: String
        requires :name, type: String
        requires :location, type:String
        requires :servicecharge, type:String
        requires :gst, type:String
        requires :contactinfo, type:String
      end
      put ':id' do
        Merchant.find(params[:id]).update({
		    name:params[:name],
	      location:params[:location],
	      servicecharge:params[:servicecharge],
	      gst:params[:gst],
	      contactinfo:params[:contactinfo]
    		})
      end
    end
  end
end
