module Userdata
  class Data < Grape::API
    resource :userdata_data do
      desc "list"
      get do
        User.all
      end
 
      desc "create a new user"
      params do
        requires :name, type: String
        requires :email, type:String
      end
      post do
        User.create!({
          name:params[:name],
          email:params[:email],
        })
      end

      desc "delete an user"
      params do
        requires :id, type: String 
      end
      delete ':id' do
        User.find(params[:id]).destroy!
      end

      desc "update an user email"
      params do 
        requires :id, type: String
        requires :email, type:String
      end
      put ':id' do
        User.find(params[:id]).update({
    		  email:params[:email]
    		})
      end
    end
  end
end
