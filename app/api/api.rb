class API < Grape::API
  prefix 'api'
  version 'v1', using: :path
  mount Userdata::Data
  mount Merchantdata::Data

  	resource :upload do
	    post do
	        receipt = params[:receipt]

	        attachment = {
	            :filename => receipt[:filename],
	            :type => receipt[:type],
	            :headers => receipt[:head],
	            :tempfile => receipt[:tempfile]
	        }

	        receipt = Receipt.new
	        receipt.receipt = ActionDispatch::Http::UploadedFile.new(attachment)
	        receipt.receipt_path = attachment[:filename]
	        receipt.name = "receipt"
	        receipt.save
	    end
	end
end
